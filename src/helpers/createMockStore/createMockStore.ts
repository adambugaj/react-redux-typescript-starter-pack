import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import services from "@/services/services";
import { Services } from "@/types/services.types";

const defaultMiddlewares = (services: Services) => [
    thunk.withExtraArgument(services),
];

const createMockStore = (initialData = {}) =>
    configureStore(defaultMiddlewares(services()))(initialData);

export default createMockStore;
