import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware, compose, Store } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import dotenv from "dotenv";
import services from "@/services/services";
import rootReducer from "@/store/rootReducer";
import { AppStore } from "@/types/store.types";
import App from "./App";
import "./styles/index.scss";

dotenv.config();
const isDevelopmentMode = process.env.NODE_ENV === "development";

const configureStoreDEV = (): Store<AppStore> =>
    createStore(
        rootReducer,
        compose(applyMiddleware(thunk.withExtraArgument(services()), logger))
    );

const configureStorePROD = (): Store<AppStore> =>
    createStore(
        rootReducer,
        compose(applyMiddleware(thunk.withExtraArgument(services())))
    );

const store: Store<AppStore> = isDevelopmentMode
    ? configureStoreDEV()
    : configureStorePROD();

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById("root")
);
