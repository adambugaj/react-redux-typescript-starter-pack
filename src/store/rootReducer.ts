import { combineReducers, Reducer } from "redux";
import { AppStore } from "@/types/store.types";

const rootReducer: Reducer<AppStore> = combineReducers({});

export default rootReducer;
