const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const FaviconsWebpackPlugin = require("favicons-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const LodashModuleReplacementPlugin = require("lodash-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const srcPath = subDir => path.join(__dirname, "src", subDir);

module.exports = {
    entry: "./src/index.tsx",
    devtool: "source-map",
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        modules: [path.resolve("./node_modules"), path.resolve("./src")],
        alias: {
            "@/helpers": srcPath("helpers"),
            "@/constants": srcPath("constants"),
            "@/containers": srcPath("containers"),
            "@/components": srcPath("components"),
            "@/labels": srcPath("labels"),
            "@/services": srcPath("services"),
            "@/store": srcPath("store"),
            "@/types": srcPath("types"),
        },
    },
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()],
    },
    output: {
        path: path.join(__dirname, "/dist"),
        filename: "bundle.min.js",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader",
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: "style-loader",
                    },
                    {
                        loader: "css-loader",
                    },
                    {
                        loader: "sass-loader",
                    },
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/static/index.html",
        }),
        new MiniCssExtractPlugin({
            filename: "bundle.css",
        }),
        new FaviconsWebpackPlugin("./src/static/favicon.png"),
        new LodashModuleReplacementPlugin(),
    ],
    node: {
        fs: "empty",
    },
};
